﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace JeffreyWebsearchPlugin
{
    public partial class AddSearch : Form
    {
        internal WebSearch FilledSearch { get; private set; }

        private WebSearch engine = new WebSearch();
        private WebSearch original = null;
        private Regex urlMatch = new Regex(@"(?:(?:http(?:s?)):\/\/)?(?:\w+\.)*(\w+)\.\w+(?:[\/\?]?|$)", RegexOptions.Compiled);

        private bool canChangeName = false;
        private bool canChangeDisplayText = false;

        public AddSearch()
        {
            this.InitializeComponent();
        }
        internal AddSearch(WebSearch search) : this()
        {
            this.original = search;

            this.txtTitle.Text = search.displayText;
            this.txtUrl.Text = search.url;
            this.chkEncode.Checked = search.encodeSpaces;

            if (search.icon != null)
                this.dropZone.BackgroundImage = Bitmap.FromHicon(search.icon.Handle);

            this.canChangeName = true;
            this.canChangeDisplayText = true;
        }

        private void dropZone_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void dropZone_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                Array a = (Array)e.Data.GetData(DataFormats.FileDrop);
                if (a != null)
                {
                    string s = a.GetValue(0).ToString();
                    if (!String.IsNullOrWhiteSpace(s) && File.Exists(s))
                    {
                        try
                        {
                            Bitmap bmp = (Bitmap)Bitmap.FromFile(s);
                            if (bmp.Size.Width > 50 || bmp.Size.Height > 50)
                            {
                                bmp = new Bitmap(bmp, new Size(50, 50));
                            }
                            dropZone.BackgroundImage = bmp;

                            IntPtr iconHandle = bmp.GetHicon();
                            engine.icon = System.Drawing.Icon.FromHandle(iconHandle);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            engine.name = txtName.Text;
            engine.displayText = txtTitle.Text;
            engine.url = txtUrl.Text;
            engine.encodeSpaces = chkEncode.Checked;

            if (original != null)
            {
                if (engine.icon == null)
                    engine.icon = original.icon;

                engine.active = original.active;
                engine.custom = original.custom;
            }
            else
            {
                engine.active = true;
                engine.custom = true;
            }

            FilledSearch = engine;
        }

        private void txtUrl_TextChanged(object sender, EventArgs e)
        {
            string name = null;

            Match m = urlMatch.Match(txtUrl.Text);
            if (m.Groups.Count > 1)
            {
                name = m.Groups[1].Value;
                if (!String.IsNullOrWhiteSpace(name) && name.Length > 0) {
                    name = String.Format("{0}{1}", Char.ToUpper(name[0]), name.Substring(1));
                }
            }

            if (!String.IsNullOrWhiteSpace(name))
            {
                if (canChangeName || String.IsNullOrWhiteSpace(txtName.Text))
                {
                    canChangeName = true;
                    txtName.Text = name;
                }

                if (canChangeDisplayText || String.IsNullOrWhiteSpace(txtTitle.Text))
                {
                    canChangeDisplayText = true;
                    txtTitle.Text = String.Format("Search {0} for '{{query}}'", name);
                }
            }
        }

        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            canChangeName = false;
        }

        private void txtTitle_KeyPress(object sender, KeyPressEventArgs e)
        {
            canChangeDisplayText = false;
        }
    }
}
