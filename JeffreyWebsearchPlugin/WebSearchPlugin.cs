﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using JeffreyPlease;
using JeffreyPlease.Logic;
using JeffreyPlease.Plugin;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using JSONdotNET;

namespace JeffreyWebsearchPlugin
{
    public class WebSearchPlugin : JeffreyExtension
    {
        public override string Name { get { return "Jeffrey Websearch plugin"; } }
        public override string Description { get { return "Tell Jeffrey to search the web for you..."; } }

        private ThemeManager thememanager;
        private SettingsManager settingsManager;
        private JSONObject settings;
        internal List<WebSearch> websearchEngines = new List<WebSearch>();

        public WebSearchPlugin()
        {
            thememanager = Store.Get<ThemeManager>();
            settingsManager = Store.Get<SettingsManager>();

            AppStore appStore = Store.Get<AppStore>();
            appStore.SearchHook_After += new SearchHookEventHandler(appStore_SearchHook_After);

            LoadCustomWebSearches();


            settings = ProvideSettings();
            DirectoryInfo res = ProvideSettingsFolder();

            if (settings.ContainsKey("generic"))
            {
                foreach (var key in settings["generic"].Keys)
                {
                    bool value = settings["generic"].GetBool(key);
                    foreach (WebSearch s in websearchEngines)
                    {
                        if (s.name.Equals(key))
                        {
                            s.active = value;
                        }
                    }
                }
            }

            if (settings.ContainsKey("engines") && settings["engines"] is JSONArray)
            {
                JSONArray engines = settings["engines"] as JSONArray;
                foreach (JSONObject obj in engines)
                {
                    WebSearch engine = WebSearch.CreateFromJSON(obj);
                    FileInfo icon = new FileInfo(Path.Combine(res.FullName, String.Format("{0}.ico", engine.name)));
                    if (icon.Exists)
                        engine.icon = new Icon(icon.FullName);
                    websearchEngines.Add(engine);
                }
            }
        }

        public void appStore_SearchHook_After(SearchHookEventArgs ev)
        {
            foreach(WebSearch engine in websearchEngines)
            {
                if (engine.active)
                {
                    string displayText = engine.displayText.Replace("{query}", ev.Pattern);
                    string url = engine.url.Replace("{query}", (engine.encodeSpaces ? ev.Pattern.Replace(' ', '+') : ev.Pattern));
                    ev.Result.Add(new WebSearchApp(displayText, "", url, engine.icon));
                }
            }
        }

        public override UserControl GetSettingsPanel()
        {
            JSONObject genericOptions;
            JSONArray engines;

            if (settings.ContainsKey("generic"))
                genericOptions = settings["generic"];
            else
            {
                genericOptions = new JSONObject();
                settings.Add("generic", genericOptions);
            }

            if (settings.ContainsKey("engines") && settings["engines"] is JSONArray)
                engines = settings["engines"] as JSONArray;
            else
            {
                engines = new JSONArray();
                settings.Add("engines", engines);
            }

            return new SettingsPanel(this, genericOptions, engines, ProvideSettingsFolder());
        }

        private Icon LoadIcon(string path)
        {
            Icon icon = null;
            if (!String.IsNullOrWhiteSpace(path))
            {
                Stream iconStream = settingsManager.OpenRead(path);
                if (iconStream != null)
                {
                    Bitmap bmp = new Bitmap(iconStream);
                    iconStream.Close();
                    icon = Icon.FromHandle(bmp.GetHicon());
                }
            }
            return icon;
        }

        private void LoadCustomWebSearches()
        {
            /* Google */
            websearchEngines.Add(new WebSearch()
            {
                name = "Google",
                displayText = "Search google for '{query}'",
                encodeSpaces = true,
                active = true,
                url = "https://www.google.de/search?q={query}",
                icon = IconResource.google
            });
            /* Wikipedia */
            websearchEngines.Add(new WebSearch()
            {
                name = "Wikipedia",
                displayText = "Search wikipedia for '{query}'",
                active = true,
                encodeSpaces = true,
                url = "http://de.wikipedia.org/w/index.php?search={query}",
                icon = IconResource.wiki
            });
            /* Yahoo */
            websearchEngines.Add(new WebSearch()
            {
                name = "Yahoo",
                displayText = "Search yahoo for '{query}'",
                encodeSpaces = true,
                url = "https://de.search.yahoo.com/search?p={query}",
                icon = IconResource.yahoo
            });
            /* Bing */
            websearchEngines.Add(new WebSearch()
            {
                name = "Bing",
                displayText = "Search bing for '{query}'",
                encodeSpaces = true,
                url = "https://www.bing.com/search?q={query}",
                icon = IconResource.bing
            });
            /* Imdb */
            websearchEngines.Add(new WebSearch()
            {
                name = "Imdb",
                displayText = "Search imdb for '{query}'",
                encodeSpaces = true,
                url = "http://www.imdb.com/find?q={query}&s=all",
                icon = IconResource.imdb
            });
        }
    }
}
