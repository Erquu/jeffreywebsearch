﻿using System.Drawing;
using JSONdotNET;

namespace JeffreyWebsearchPlugin
{
    internal class WebSearch
    {
        public static WebSearch CreateFromJSON(JSONObject json)
        {
            WebSearch s = new WebSearch();

            if (json.ContainsKey("name"))
                s.name = json.GetString("name");
            if (json.ContainsKey("displayText"))
                s.displayText = json.GetString("displayText");
            if (json.ContainsKey("url"))
                s.url = json.GetString("url");
            if (json.ContainsKey("encodeSpaces"))
                s.encodeSpaces = json.GetBool("encodeSpaces");
            if (json.ContainsKey("active"))
                s.active = json.GetBool("active");

            s.engineObject = json;
            s.custom = true;

            return s;
        }

        public string name;
        public string displayText;
        public string url;
        public bool encodeSpaces;
        public bool active;
        public bool custom;
        public Icon icon;

        public JSONObject engineObject;

        public void CopyFrom(WebSearch c)
        {
            this.name = c.name;
            this.displayText = c.displayText;
            this.url = c.url;
            this.encodeSpaces = c.encodeSpaces;
            this.active = c.active;
            this.custom = c.custom;
            this.icon = c.icon;
        }

        public void UpdateJson()
        {
            if (engineObject == null)
                engineObject = new JSONObject();

            engineObject.Add("name", name);
            engineObject.Add("displayText", displayText);
            engineObject.Add("url", url);
            engineObject.Add("encodeSpaces", encodeSpaces);
            engineObject.Add("active", active);
        }
    }
}
