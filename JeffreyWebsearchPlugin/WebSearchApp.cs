﻿using System.Diagnostics;
using JeffreyPlease.Logic.Apps.Static;
using JeffreyPlease.Logic;
using JeffreyPlease.Logic.Apps;
using System.Drawing;
using System.IO;

namespace JeffreyWebsearchPlugin
{
    public class WebSearchApp : DefaultApp
    {
        private string url;

        public override Icon Icon { get; protected set; }

        public WebSearchApp(string name, string description, string url, Icon icon)
        {
            this.Name = name;
            this.Description = description;

            this.url = url;

            this.Icon = icon;
        }

        public override void Run()
        {
            Process.Start(this.url);
        }
    }
}
