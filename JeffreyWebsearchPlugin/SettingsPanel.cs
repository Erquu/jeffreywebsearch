﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using JSONdotNET;
using System.IO;

namespace JeffreyWebsearchPlugin
{
    public partial class SettingsPanel : UserControl
    {
        private ListViewGroup custom = new ListViewGroup("Custom");
        private ListViewGroup generic = new ListViewGroup("Generic");

        private WebSearchPlugin plugin;
        private JSONObject genericOptions;
        private JSONArray plugins;
        private DirectoryInfo resourceDir;

        public SettingsPanel(WebSearchPlugin plugin, JSONObject genericOptions, JSONArray pluginList, DirectoryInfo res)
        {
            InitializeComponent();

            this.plugin = plugin;
            this.genericOptions = genericOptions;
            this.plugins = pluginList;
            this.resourceDir = res;

            listView1.Groups.AddRange(new ListViewGroup[] { custom, generic });

            if (this.plugin != null)
            {
                foreach (WebSearch engine in this.plugin.websearchEngines)
                {
                    ListViewItem item = new ListViewItem();
                    item.Tag = engine;
                    item.Group = engine.custom ? custom : generic;
                    item.Checked = engine.active;
                    item.SubItems.Add(engine.name);
                    item.SubItems.Add(engine.displayText);

                    listView1.Items.Add(item);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddSearch add = new AddSearch();
            if (add.ShowDialog() == DialogResult.OK)
            {
                WebSearch engine = add.FilledSearch;

                ListViewItem item = new ListViewItem();
                item.Tag = engine;
                item.Group = custom;
                item.Checked = engine.active;

                item.SubItems.Add(engine.name);
                item.SubItems.Add(engine.displayText);

                listView1.Items.Add(item);

                this.plugin.websearchEngines.Add(engine);

                engine.UpdateJson();
                this.plugins.Add(engine.engineObject);

                if (engine.icon != null)
                {
                    FileInfo icon = new FileInfo(Path.Combine(resourceDir.FullName, String.Format("{0}.ico", engine.name)));
                    FileStream iconStream = icon.OpenWrite();
                    engine.icon.Save(iconStream);
                    iconStream.Close();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                ListViewItem item = listView1.SelectedItems[0];
                object tag = item.Tag;
                if (tag is WebSearch)
                {
                    WebSearch engine = (WebSearch)tag;
                    AddSearch change = new AddSearch(engine);
                    if (change.ShowDialog() == DialogResult.OK)
                    {
                        WebSearch changedEngine = change.FilledSearch;
                        
                        FileInfo icon = new FileInfo(Path.Combine(resourceDir.FullName, String.Format("{0}.ico", engine.name)));
                        if (icon.Exists)
                        {
                            icon.Delete();
                        }

                        engine.CopyFrom(changedEngine);
                        engine.UpdateJson();


                        icon = new FileInfo(Path.Combine(resourceDir.FullName, String.Format("{0}.ico", engine.name)));
                        if (engine.icon != null)
                        {
                            FileStream iconStream = icon.OpenWrite();
                            engine.icon.Save(iconStream);
                            iconStream.Close();
                        }

                        item.Checked = engine.active;
                        item.SubItems[1].Text = engine.name;
                        item.SubItems[2].Text = engine.displayText;
                        item.Checked = engine.active;
                    }
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                object tag = listView1.SelectedItems[0].Tag;
                if (tag is WebSearch)
                {
                    WebSearch engine = (WebSearch)tag;
                    btnEdit.Enabled = engine.custom;
                }
            }
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                ListViewItem item = listView1.SelectedItems[0];
                object tag = item.Tag;
                if (tag is WebSearch)
                {
                    WebSearch engine = (WebSearch)tag;
                    btnEdit.Enabled = engine.custom;
                }
            }
        }

        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Debug.WriteLine("Item Checked change");
            if (e.Item.Tag is WebSearch)
            {
                Debug.WriteLine("Item is valid");
                WebSearch engine = (WebSearch)e.Item.Tag;
                engine.active = e.Item.Checked;

                if (engine.custom)
                    engine.UpdateJson();
                else
                {
                    this.genericOptions.Add(engine.name, engine.active);
                }
            }
        }
    }
}
